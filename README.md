Spam All the Links
==================

Spam All the Links is a command line script that fetches URL suggestions from
several sources and assembles them into one email. That email can in turn be
pasted into a blog entry or otherwise used to share the list of links.

Linkspamming
============

Use case
--------

Spam All the Links was written to assist in producing the [Geek Feminism
linkspam posts](http://geekfeminism.org/tag/linkspam/). It was developed to
check WordPress comments, bookmarking websites such as Pinboard, and Twitter,
for links tagged "geekfeminism", assemble them into one email, and email them
to an editor who could use the email as the basis for a blog post.

The script has been generalised to allow searches of RSS/Atom feeds, Twitter, and
WordPress blog comments as specified by a configuration file.

Email output
------------

The email output of the script has three components:

1. a plain text email with the list of links
2. a HTML email with the list of links
3. an attachment with the HTML formatted links but no surrounding text so as to be easily copy and pasted

All three parts of the email can be templated with Jinja2.

Sources of links
----------------

Spam All the Links currently can be configured to check multiple sources of links, in these forms:

1. RSS/Atom feeds, such as those produced by the bookmarking sites [Pinboard](https://pinboard.in/u:puzzlement) or [Diigo](https://www.diigo.com/), where the link, title and description of the link can be derived from the equivalent fields in the RSS/Atom. (`bookmarkfeed` in the configuration file)
2. RSS/Atom feeds where links can be found in the 'body' of a post (`postfeed` in the configuration file)
3. Twitter searches (`twitter` in the configuration file)
4. comments on WordPress blog entries (`wpcommentsfeed` in the configuration file)

Link processing
---------------

Spam All the Links post-processes links before sending out the email. It:
* attempts to follow redirects to a final link destination, in order to avoid URL shorteners and allow you to link directly to the original URL
* in the case of links sourced from Twitter, uses the Twitter API to get the original URL rather than the t.co URL
* strips UTM tracking queries from URLs
* other than where a title is already provided (eg, bookmarking sites), attempts to access the URL to determine the title of the page

Installing and running Spam All the Links
=========================================

Requirements
------------

Spam All the Links is a Python 3 script. It additionally requires these Python
modules:
* [feedparser](https://pypi.python.org/pypi/feedparser)
* [beautifulsoup4](https://pypi.python.org/pypi/beautifulsoup4)
* [twitter](https://pypi.python.org/pypi/twitter)
* [Jinja2](https://pypi.python.org/pypi/Jinja2)

The modules are listed in `requirements.txt` at the root of the repository and can be installed with pip.

Templating
----------

Each of the three parts of the email sent by Spam All the Links can be
templated. The script provides a small amount of data to the Jinja2 templating
engine. The variables available are:
* `link` objects, which have two properties: `link.url` and `link.title`. Some links may also have a description: `link.description`.
* `links`, a list of `link` objects

An example Jinja template for outputting this information as an unnumbered HTML list is:

    <ul>
    {% for link in links %}
    <li><a href="{{ link.url }}">{{ link.title }}</a>{% if link.description %}: {{
    link.description }}{% endif %}</li>
    {% endfor %}
    </ul>

In the configuration file, specify a template directory `templatedir` containing three files:
1. `mail.txt`, the Jinja template for the plain text email
2. `mail.html`, the Jinja template for the HTML email
3. `copypasta.html`, the Jinja template for the HTML-formatted attachment

For Windows machines, the file path will need to be escaped, as below:
````
"templatedir": "C:\\Users\\zach\\Documents\\templates",
````
Example templates can be found in the `docs/examples/templates` section of the repository.

For Jinja documentation see [Template Designer Documentation](http://jinja.pocoo.org/docs/dev/templates/).

Usage
-----

The main script is `mailout.py`. Usage is:

    usage: mailout.py [-h] [--days N] [--no-empty-mail] [--subject SUBJECT]
                      [--from-addr FROM_ADDR] [--from-name FROM_NAME]
                      to [to ...]
    
    positional arguments:
      to                    Recipient email addresses
    
    optional arguments:
      -h, --help            show this help message and exit
      --days N, -d N, --days N
                            email links in the last N days (default 5)
      --no-empty-mail, -e   raise an error rather than send an empty email
      --subject SUBJECT, -s SUBJECT
                            Subject line of generated email
      --from-addr FROM_ADDR, -f FROM_ADDR
                            From address eg "me@example.com"
      --from-name FROM_NAME, -n FROM_NAME
                            From name eg "First Last"

Configuration file
------------------

Link sources are configures in the `~/.spamallthelinks.json` file, and are in
JSON format. An example configuration file is in
`docs/examples/config/spamallthelinks.json` in the repository.

To configure *sources* of links, ie, places to obtain link suggestions from, use the sources section:

    "sources": {
        "bookmarkfeed": {
            "urls": [
                "https://feeds.pinboard.in/rss/t:kangaroo",
                "https://www.diigo.com/rss/tag/kangaroo?tab=153"
            ]
        },
        "twitter": {
            "searches": [
                "kangaroo",
                "joey"
            ]
        },
        "wpcommentsfeed": {
            "urls": [
                "http://example.com/tag/kangaroos/feed/"
            ]
        }
    },

In this example, there are five sources of links:
1. the bookmark-style RSS feed https://feeds.pinboard.in/rss/t:kangaroo
1. the bookmark-style RSS feed https://www.diigo.com/rss/tag/kangaroo?tab=153
1. links posted to Twitter that are found on a search for "kangaroo"
1. links posted to Twitter that are found on a search for "joey"
1. links posted *in the comments* of WordPress entries found at http://example.com/tag/kangaroos/feed/

To configure *excluded* links, use the `exclusions` section. Even if links are
found in the sources, if they are also present in an exclusion, they will not
be included in the email.

    "exclusions": {
        "postfeed": {
            "urls": [
                "http://example.com/tag/kangaroos/feed/"
            ]
        }
    },

One possible use of the `exclusions` section is to exclude links you have
already posted somewhere, eg you could list your own blog or Twitter feed as an
exclusion.

*Warning*: exclusions are based on _exact_ URL matches. If the same content
appears at several URLs, it may continue to be suggested in future mailouts if
one version of the URL appears in an exclusion and another in a source.

All four options — `bookmarkfeed`, `twitter`, `postfeed` and `wpcommentsfeed` —
can be used in either or both the `sources` or the `exclusions` section. You
may use as many URLs or Twitter searches as you need in each configuration
item.

SMTP configuration
------------------

The SMTP host your email will be sent through can also be configured in the `~/.spamallthelinks.json` configuration file. As an example:

    "smtp": {
        "host": "mail.example.com",
        "port": 587,
        "tls": true,
        "user": "username",
        "pass": "t0ps3kr1t"
    }

The entire `smtp` section of the configuration file is optional; by default
messages will be sent via `localhost` on port `25` without transport
encryption. Options are:

* `host`: outgoing SMTP hostname
* `port`: outgoing SMTP port number (as an integer)
* `tls`: use TLS encrypton (`true` or `false`)
* `user`: username to authenticate to the SMTP server as
* `pass`: password to authenticate to the SMTP server with

Twitter credentials
-------------------

To obtain Twitter credentials to use the Twitter API, you must register Spam
All the Links as a Twitter application yourself. To do so, go to
[apps.twitter.com](https://apps.twitter.com/) and select 'Create New App'. Fill
in appropriate values for Name, Description and Website.

To get the appropriate values for the `key` and `secret` components of the
configuration file, go to [apps.twitter.com](https://apps.twitter.com/), select
your registered application, and select the 'Keys and Access Tokens' tab. The
`key` value is listed as "Consumer Key (API Key)" and the `secret` value as
"Consumer Secret (API Secret)".

Spam All the Links only reads from Twitter, it does not write to it. You can
set the app Permissions to 'Read only'.

If you are not using the `twitter` link source as either a source or an
exclusion, you can leave the `twitterauth` section out of the config file and
do not need to register a Twitter application.

About
=====

Credits
-------

Spam All the Links was developed by [Mary
Gardiner](http://mary.gardiner.id.au/) for the [Geek
Feminism](http://geekfeminism.org/) website.

Licence
-------

Spam All the Links is free software available under the MIT licence. See
LICENCE for full details.