import datetime, smtplib, sys
from email.mime import multipart as mm, text as mt
from argparse import ArgumentParser
from build import build_links, NoLinksException

import config as jsonconfig
import configconstants as CC

DAYS = 5

def parseArgs():
    parser = ArgumentParser()
    parser.add_argument("--days", "-d", "--days", type=int,
            help="email links in the last N days (default %d)" % DAYS,
            metavar="N", default=DAYS)
    parser.add_argument("--no-empty-mail", "-e", action="store_true",
            help="raise an error rather than send an empty email",
            default=False)
    parser.add_argument("--subject", "-s", 
            help="Subject line of generated email")
    parser.add_argument("--from-addr", '-f',
            help="From address eg \"me@example.com\"")
    parser.add_argument("--from-name", '-n',
            help="From name eg \"First Last\"")
    parser.add_argument("to", help = "Recipient email addresses", nargs="+")
    options = parser.parse_args()

    return options

def getMsg(to, from_addr, from_name, subject, links):
    msg = mm.MIMEMultipart()
    msg["To"] = ", ".join(to)
    if from_addr is not None:
        if from_name is not None:
            msg["From"] = "%s <%s>" % (from_name, from_addr)
        else:
            msg["From"] = "%s" % from_addr
    if subject is not None:
        msg["Subject"] = subject    

    main_body = mm.MIMEMultipart("alternative")

    try:
        main_body.attach(mt.MIMEText(links.renderText(), 'plain', "utf-8"))
        main_body.attach(mt.MIMEText(links.renderHTMLPage(), 'html', "utf-8"))
        source_mode = mt.MIMEText(links.renderCopyPastaHTML(), 'plain', "utf-8")
    except NoLinksException:
        print("Error: no links found, and --no-empty-mail option set", file=sys.stderr)
        print("Aborting", file=sys.stderr)
        sys.exit(1)

    source_mode.add_header('Content-Disposition', 'attachment; filename="source mode version (copy and pastable HTML).txt"')

    msg.attach(main_body)

    msg.attach(source_mode)

    return msg

SMTP_DEFAULT_HOST = 'localhost'
SMTP_DEFAULT_PORT=0

def sendMsg(from_, to, msg, config):
    smtp_config = config.get(CC.SMTP, {})

    with smtplib.SMTP(host = smtp_config.get(CC.SMTP_HOST,
                SMTP_DEFAULT_HOST),
            port = smtp_config.get(CC.SMTP_PORT, SMTP_DEFAULT_PORT)
        ) as s:
        try:
            if smtp_config[CC.SMTP_TLS]:
                s.starttls()
        except KeyError:
            pass

        try:
            s.login(smtp_config[CC.SMTP_USER], smtp_config[CC.SMTP_PASS])
        except KeyError:
            pass

        s.sendmail(from_, to, msg.as_string())

def main():
    options = parseArgs()

    config = jsonconfig.parse_config()

    links = build_links(config, options)

    msg = getMsg(options.to, options.from_addr, options.from_name,
            options.subject, links)
    sendMsg(options.from_addr, options.to, msg, config)

if __name__ == '__main__':
    main()

