import itertools
import feedparser
import linksinfeed
from bookmarks import URLIterator, LinkDetails, is_recent

COMMENT_FEED_SUFFIX='feed/'

class WPCommentsURLIterator(URLIterator):
    def __init__(self, url, url_cache):
        URLIterator.__init__(self, url_cache)
        self.posts_feed = url

    def _test(self, entry):
        return True

    def _construct_post_iterator(self, url):
        return linksinfeed.FeedContentsURLIterator(
                url, self.url_cache)

    def fetch(self):
        self.iterator = itertools.chain.from_iterable([
            self._construct_post_iterator(
                        entry.link + COMMENT_FEED_SUFFIX)
                    for entry in feedparser.parse(self.posts_feed)['entries']
                    if self._test(entry)])

    def __next__(self):
        return next(self.iterator)

class WPCommentsDetailIterator(WPCommentsURLIterator, LinkDetails):

    def __init__(self, url, now, days, url_cache, title_cache):
        WPCommentsURLIterator.__init__(self, url, url_cache)
        LinkDetails.__init__(self, now, days, title_cache)

    def _test(self, entry):
        return is_recent(self.now, self.days, entry)

    def _construct_post_iterator(self, url):
        return linksinfeed.FeedContentsDetailsIterator(
                url, self.now, self.days, self.url_cache, self.title_cache)
