HTML_TMPL = "mail.html"
TEXT_TMPL = "mail.txt"
PASTA_TMPL = "copypasta.html"

URLS="urls"

SOURCES="sources"

BOOKMARKS="bookmarkfeed"

TWITTER="twitter"
SEARCHES="searches"

WORDPRESS_POSTS_WITH_COMMENTS="wpcommentsfeed"

EXCLUSIONS="exclusions"
RSSPOSTS="postfeed"

TWITTER_AUTH="twitterauth"
KEY="key"
SECRET="secret"

SMTP = "smtp"
SMTP_HOST = "host"
SMTP_USER = "user"
SMTP_PASS = "pass"
SMTP_PORT = "port"
SMTP_TLS = "tls"
