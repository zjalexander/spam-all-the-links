import json, os

CONFIGFILE = os.path.expanduser("~/.spamallthelinks.json")

def parse_config(filename = CONFIGFILE):
    return json.load(open(filename))

def main():
    print(parse_config())

if __name__ == '__main__':
    main()
