import bs4, feedparser
from bookmarks import URLIterator, LinkDetails, is_recent

class FeedContentsURLIterator(URLIterator):
    def __init__(self, feed_url, url_cache):
        URLIterator.__init__(self, url_cache)
        self.feed_url = feed_url
        self.items = set()

    def fetch(self):
        self.entries = feedparser.parse(self.feed_url)['entries']

    def _append_item(self, url):
        self.items.add(self.url_cache[url])

    def processEntry(self, entry):
        if not self._test(entry):
            return
        for content in entry.content:
            soup = bs4.BeautifulSoup(content.value, "html.parser")
            for a in soup.find_all('a'):
                try:
                    self._append_item(a['href'])
                except KeyError:
                    pass

    def __next__(self):
        while len(self.items) == 0 and len(self.entries) > 0:
            entry = self.entries.pop()
            self.processEntry(entry)
        try:
            return self.items.pop()
        except KeyError:
            raise StopIteration

class FeedContentsDetailsIterator(FeedContentsURLIterator, LinkDetails):

    def __init__(self, feed_url, now, days, url_cache, title_cache):
        FeedContentsURLIterator.__init__(self, feed_url, url_cache)
        LinkDetails.__init__(self, now, days, title_cache)

    def _append_item(self, url):
        self.items.add((self.url_cache[url], self.title_cache[url], None))

    def _test(self, entry):
        return is_recent(self.now, self.days, entry)
